const source = 'this';
module.exports = {
  pipeline: [`${source}:jazz-plugin-textloader:load`],
  plugins: [
    {
      name: 'jazz-plugin-textloader',
      tasks: {
        load: {
          getRawData: () => ['teste', 'teste2', 'teste3', 'teste4'],
        },
      },
    },
  ],
};
