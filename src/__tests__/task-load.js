/* eslint-disable import/no-extraneous-dependencies */
import { createConfigInfo } from 'jazz-core/dist/helpers';
import LoaderTask from '../LoaderTask';

const configFile = './src/__tests__/pack-config';
const config = createConfigInfo(configFile);
const task = new LoaderTask('load', { target: './src/__tests__/target.txt' }, config);

const data = ['row-test1', 'row-test2'];

task.setRawData(data);
task.execute(data);
