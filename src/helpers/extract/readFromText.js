import fs from 'fs';

const readFromText = (sourceFile) => {
  const data = fs.createReadStream(sourceFile);
  return data;
};

module.exports = readFromText;
