/* eslint-disable class-methods-use-this */
import fs from 'fs';
import Task from './Task';
import { name as pluginName } from './config';

class LoaderTask extends Task {
  constructor(id, params, config, description = '', rawDataFrom = null) {
    super(id, params, config, description, rawDataFrom);
    this.outputFile = params.target;
    [this.plugin] = this.config.plugins.filter(plugin => plugin.name === pluginName);
    LoaderTask.validateParameters(params);
  }

  validateConditionsForExecution() {
    if (!this.rawData) {
      return false;
    }
    return true;
  }

  execute(data) {
    this.rawData = data || this.getRawData();

    const CRLF = '\r\n';
    const { outputFile } = this;

    if (!this.validateConditionsForExecution()) return;

    let newData = '';

    this.rawData.map((row) => {
      if (newData.length > 0) newData = newData.concat(CRLF);
      newData = newData.concat(row);
      return newData;
    });

    fs.writeFile(outputFile, newData, (err) => {
      if (err) {
        this.onError(err);
      }
    });

    this.onSuccess(outputFile);

    return {
      outputFile,
      data: newData,
    };
  }

  onSuccess(outputFile) {
    this.logger.success(`File generated successfully: ${outputFile}`);
    super.onSuccess(outputFile);
  }

  static validateParameters({ target }) {
    if (!target) {
      this.onError('Target Info is required');
    }
  }
}
module.exports = LoaderTask;
