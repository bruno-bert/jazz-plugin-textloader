import path from 'path';

export const name = 'jazz-plugin-textloader';
export const pipeline = [
  {
    id: 'load',
    description: ' Loads json object into text file',
    class: 'LoaderTask',
    params: ['target'],
  },
];

export const inputParameters = {
  outputFile: {
    alias: 'o',
    describe: 'Output File',
    demandOption: true,
    default: path.join(path.resolve(process.cwd()), 'target.txt'),
    name: 'target',
  },
};
