# Jazz Plugin: Text Loader Documentation

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.


## Plugin Pipeline

### Tasks

Contains a single task, named _load_.

#### load

**Purpose**: _load_ task is responsable for loading the received data into a text file.

**Class**: LoaderTask

**Parameters**:

- target: represents the path of the output text file. References to the inputParameter _outputFile_.

```javascript
export const pipeline = [
  {
    id: "load",
    description: " Loads json object into text file",
    class: "LoaderTask",
    params: ["target"]
  }
];
```

### Input Parameters

```javascript
export const inputParameters = {
  outputFile: {
    alias: "o",
    describe: "Output File",
    demandOption: true,
    default: path.join(path.resolve(process.cwd()), "target.txt"),
    name: "target"
  }
};
```

- outputFile: indicates the path in which the output file will be generated at the end of the process. Default is the _{current path} + /target.txt_. References to the parameter named _target_ in the LoaderTask Class.
